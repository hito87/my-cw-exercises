# https://www.codewars.com/kata/52f831fa9d332c6591000511
'''
For a given chemical formula represented by a string, count the number of atoms 
of each element contained in the molecule and return an object

Some formulas have brackets in them. The index outside the brackets tells you that 
you have to multiply count of each atom inside the bracket on this index. For example, 
in Fe(NO3)2 you have one iron atom, two nitrogen atoms and six oxygen atoms.

Note that brackets may be round, square or curly and can also be nested. Index 
after the braces is optional.
'''
import re
def parse_molecule (formula):
    formula, d = formula.replace('[', '(').replace(']', ')').replace('{', '(').replace('}', ')'), {}
    reg = re.findall(r'(\(([A-Za-z0-9]+)\)([0-9]+))', formula)
    while len(reg) > 0:
        formula = formula.replace(reg[0][0], reg[0][1] * int(reg[0][2]))
        reg = re.findall(r'(\(([A-Za-z0-9]+)\)([0-9]+))', formula)
    
    reg = re.findall(r'[A-Z0-9()][^A-Z0-9()]*', formula)
    new_reg = []
    # Treat the cases where a molecule is followed by more than two numbers, eg: O12
    for i, s in enumerate(reg):
        if s.isdigit():
            if i < len(reg)-1 and reg[i+1].isdigit():
                new_reg.append(''.join([reg[i], reg[i+1]]))
                reg.remove(reg[i+1])
            else:
                new_reg.append(s)
        else:
            new_reg.append(s)
    reg = new_reg
    for i, s in enumerate(reg):
        if s.isalpha():
            if i < len(reg)-1 and reg[i+1].isdigit():
                if s in d.keys():
                    d[s] += int(reg[i+1])
                else:
                    d[s] = int(reg[i+1])
            else:
                if s in d.keys():
                    d[s] += 1
                else:
                    d[s] = 1
    return d

# samples:
sample_strings = ['H2O',
                  'B2H6',
                  'C6H12O6',
                  'Mo(CO)6',
                  'Mg(OH)2',
                  'Fe(C5H5)2',
                  '(C5H5)Fe(CO)2CH3',
                  'Pd[P(C6H5)3]4',
                  'K4[ON(SO3)2]2',
                  'As2{Be4C5[BCo3(CO2)3]2}4Cu5',
                  '{[Co(NH3)4(OH)2]3Co}(SO4)3',
                  'C2H2(COOH)2'
                  ]

