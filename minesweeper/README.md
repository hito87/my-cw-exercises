# Minesweeper challenge

[Minesweeper kata](https://www.codewars.com/kata/mine-sweeper/python)

Solves 99% of the cases, some edge scenarios have to be customly dealt with, a LOT of refactoring can be done.