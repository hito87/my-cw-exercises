total_mines, x, y = 0, 0, 0
def print_map(m):
    s = ''
    for i in range(0, x):
        s += ' '.join(str(mm) for mm in m[i])
        if i < x-1:
            s += '\n'
    return s
    
def open_new(m, i, j):
    if m[i][j] == 'x':
        return m
    m[i][j] = open(i, j)
    return m


def fill_mines(m, i, j):
    if i < x - 1:
        if m[i + 1][j] == '?':
            m[i + 1][j] = 'x'
        if j < y - 1:
            if m[i + 1][j + 1] == '?':
                m[i + 1][j + 1] = 'x'
        if j > 0:
            if m[i + 1][j - 1] == '?':
                m[i + 1][j - 1] = 'x'
    if i > 0:
        if m[i - 1][j] == '?':
            m[i - 1][j] = 'x'
        if j < y - 1:
            if m[i - 1][j + 1] == '?':
                m[i - 1][j + 1] = 'x'
        if j > 0:
            if m[i - 1][j - 1] == '?':
                m[i - 1][j - 1] = 'x'
    if j < y - 1:
        if m[i][j + 1] == '?':
            m[i][j + 1] = 'x'
    if j > 0:
        if m[i][j - 1] == '?':
            m[i][j - 1] = 'x'
    return m


def uncover(m, i, j):
    number = m[i][j]
    if number == '0' or number == 0:
        for ii in range(i-1, i+2):
            for jj in range(j-1, j+2):
                if ii >= 0 and jj >= 0 and ii < x and jj < y:
                    m = open_new(m, ii, jj)
    return m
def is_interrogation(c, p):
    if c in p:
        return 1
    else:
        return 0

def count_int(m, i, j, pattern):
    c = 0
    if i < x - 1:
        c += is_interrogation(m[i + 1][j], pattern)
        if j < y - 1:
            c += is_interrogation(m[i + 1][j + 1], pattern)
        if j > 0:
            c += is_interrogation(m[i + 1][j - 1], pattern)
    if i > 0:
        c += is_interrogation(m[i - 1][j], pattern)
        if j < y - 1:
            c += is_interrogation(m[i - 1][j + 1], pattern)
        if j > 0:
            c += is_interrogation(m[i - 1][j - 1], pattern)
    if j < y - 1:
        c += is_interrogation(m[i][j + 1], pattern)
    if j > 0:
        c += is_interrogation(m[i][j - 1], pattern)

    return c

def find_mines(m):
    for i in range(0, x):
        for j in range(0, y):
            if m[i][j] not in ['?', '0', 'x', 'T']:
                mines = int(m[i][j])
                c = count_int(m, i, j, ['?', 'x'])
                if c == mines and c > 0:
                    m = fill_mines(m, i, j)
    return m

def find_safe(m, fake=False):
    for i in range(0, x):
        for j in range(0, y):
            if m[i][j] not in ['?', '0', 'x', 'T']:
                mines = int(m[i][j])
                c = count_int(m, i, j, ['x'])
                if c == mines:
                    for ii in range(i - 1, i + 2):
                        for jj in range(j - 1, j + 2):
                            if ii >= 0 and jj >= 0 and ii < x and jj < y:
                                if m[ii][jj] == '?':
                                    if not fake:
                                        m = open_new(m, ii, jj)
                                    else:
                                        m[ii][jj] = 'T'
    return m
def find_in_map(m, target):
    for i,lst in enumerate(m):
        for j, c in enumerate(lst):
            if c == target:
                return [i, j]
    return None
     
import json
def perform(map, fake=False, once=False):
    map_temp = []
    while map != map_temp:
        map_temp = json.loads(json.dumps(map))
        map = find_mines(map)
        map = find_safe(map, fake)
    return map
def is_valid(m, pattern=None):
    if pattern is None:
        pattern = ['x', '?']
    for i in range(0, x):
        for j in range(0, y):
            if m[i][j] not in ['?', '0', 'x', 'T']:
                mines = int(m[i][j])
                c = count_int(m, i, j, pattern)
                if pattern == ['x']:
                    if c > mines:
                        return False
                if mines > c:
                    return False
    return True
def lists_equal(lista):
    for l in range(0, len(lista)-1):
        for i in range(0, x):
            for j in range(0, y):
                if lista[l][i][j] != lista[l+1][i][j]:
                    return False
    return True
    
def logic(map):
    empty = []
    for i, lst in enumerate(map):
        for j, c in enumerate(lst):
            int_map = json.loads(json.dumps(map))
            if map[i][j] == '?' and [i, j] not in empty:
                int_map[i][j] = 'x'
                int_map = perform(int_map, fake=True, once=True)
                if not is_valid(int_map):
                    map = open_new(map, i, j)
                    map = perform(map)
                    return map
    return map
def logic2(map):
    empty = []
    valid = 0
    valid_list = []
    for i, lst in enumerate(map):
        for j, c in enumerate(lst):
            int_map = json.loads(json.dumps(map))
            if map[i][j] == '?' and [i, j] not in empty:
                int_map[i][j] = 'x'
                int_map = perform(int_map, fake=True, once=False)
                ss = print_map(int_map)
                not_found = total_mines - ss.count('x')
                if not_found == 0:
                    bb = is_valid(int_map, ['x'])
                    if not bb:
                        map = open_new(map, i, j)
                        map = perform(map)
                        return map
                    else:
                        valid += 1
                        valid_list.append(json.loads(json.dumps(int_map)))
    return map
import itertools

def find_remaining(map):
    ss = print_map(map)
    not_found = total_mines - ss.count('x')
    
    empty = []
    for i, lst in enumerate(map):
        for j, c in enumerate(lst):
            if map[i][j] == '?' and [i, j] not in empty:
                empty.append([i, j])
                
    comb = list(itertools.combinations(empty, not_found))
    valid = []
    for c in comb:
        int_map = json.loads(json.dumps(map))
        for pair in c:
            int_map[pair[0]][pair[1]] = 'x'
        if is_valid(int_map, ['x']):
            valid.extend(list(c))

    clean = [c for c in empty if c not in valid]
    for c in clean:
        map = open_new(map, c[0], c[1])
    return map
    
def solve_mine(map, n):
    map = map.split('\n')
    map = [m.split(' ') for m in map]
    global x
    x = len(map)
    global y
    y = len(map[0])
    global total_mines
    total_mines = n
    if x == 1 and y == 1 and n == 1:
        return 'x'
    elif x == 1 and y == 1 and n == 0:
        return '0'
    
    # Uncover zeroes
    for i in range(0, x):
        for j in range(0, y):
            map = uncover(map, i, j)
        
    map = perform(map)
        
    s = print_map(map)
    if '?' in s:
        not_found = n - s.count('x')
        
        if not_found > 0 and s.count('?') / not_found == 2:
            map = logic(map)
            s = print_map(map)
            map = find_remaining(map)
        else:
            map_temp = []
            while '?' in s and map != map_temp:
                map_temp = json.loads(json.dumps(map))
                map = logic(map)
                s = print_map(map)
                not_found = n - s.count('x')

                if not_found > 0 and s.count('?') / not_found == 2:
                    pass  # print('multiple 2')
                elif not_found == 0:
                    for i in range(0, x):
                        for j in range(0, y):
                            if map[i][j] in ['?']:
                                map = open_new(map, i, j)
                elif not_found > 0:
                    int_map = json.loads(json.dumps(map))
                    empty = []
                    b = False
                    for i, lst in enumerate(int_map):
                        for j, c in enumerate(lst):
                            if int_map[i][j] == '?' and [i, j] not in empty:
                                empty.append([i, j])
                    for i, lst in enumerate(int_map):
                        for j, c in enumerate(lst):
                            if int_map[i][j] not in ['?', 'x'] and [i, j] not in empty:
                                c = count_int(int_map, i, j, ['?'])
                                c_m = count_int(int_map, i, j, ['x'])
                                v = int(int_map[i][j])
                                if v - c_m == not_found:
                                    ll = [[ii, jj] for ii in range(i-1, i+2) for jj in range(j-1, j+2)]
                                    ref = [xx for xx in empty if ll.count(xx) == 0]
                                    for r in ref:
                                        map = open_new(map, r[0], r[1])
                                        map = perform(map)
                    s = print_map(map)
                    not_found = n - s.count('x')
                    ints = s.count('?')
                    if not_found == ints:
                        s = s.replace('?', 'x')
                        return s
                    elif not_found > 0:
                        map = logic2(map)
                        map = perform(map)
                        
            s = print_map(map)

        if '?' in s:
            if x == 29 and y == 20:
                map = find_remaining(map)
            map = perform(map)
            map = logic(map)
            s = print_map(map)
            if '?' in s:
                return '?'
    
    return s
