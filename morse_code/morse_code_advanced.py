from itertools import groupby
def decodeBits(bits):
    bits = bits.strip('0')
    time_unit = min(map(len, [''.join(g) for k, g in groupby(bits)]))
    return bits.replace('111'*time_unit, '-').replace('000'*time_unit, ' ').replace('1'*time_unit, '.').replace('0'*time_unit, '')#.split(' ')

def decodeMorse(morse_code):
    return ''.join([MORSE_CODE[s] if len(s) > 0 else ' ' for s in morse_code.split(' ')]).replace('  ', ' ').strip()
