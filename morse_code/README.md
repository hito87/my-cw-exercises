# Morse code series

## Basic

[Decode the Morse Code](https://www.codewars.com/kata/54b724efac3d5402db00065e)

## Advanced

[Decode the Morse code, advanced](https://www.codewars.com/kata/54b72c16cd7f5154e9000457)

## The Real deal

[Decode the Morse code, for real](https://www.codewars.com/kata/54acd76f7207c6a2880012bb)

(This one needs a lot of refactoring)