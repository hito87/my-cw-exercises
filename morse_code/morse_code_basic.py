# https://www.codewars.com/kata/54b724efac3d5402db00065e
def decodeMorse(morse_code):
    return ''.join([MORSE_CODE[s] if len(s) > 0 else ' ' for s in morse_code.split(' ')]).replace('  ', ' ').strip()
